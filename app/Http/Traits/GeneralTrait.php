<?php
namespace App\Http\Traits;

trait GeneralTrait 
{
    public function uploadFile($file , $path)
    {
        $fileName = time().'.'.$file->extension();  
     
        $file->move(public_path($path), $fileName);

        return $path . "/" . $fileName;
    }

    public function getFile($file)
    {
        return asset($file);
    }
}