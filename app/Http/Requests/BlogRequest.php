<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(Request $request,$id = NULL)
    {
        // dd($id);
        if($request->id){
            return [
                'title'       => 'required|max:50|unique:blogs,name,'.$request->id .',id,deleted_at,NULL',
                'description' => 'required',
                'image'       => 'nullable|image|max:2048',
                'status'      => 'required' 
            ];
        }else{
            return [
                'title'       => 'required|max:50|unique:blogs,name,NULL,id,deleted_at,NULL',
                'description' => 'required',
                'image'       => 'required|image|max:2048',
                'status'      => 'required' 
            ];
        }
        
    }
}
