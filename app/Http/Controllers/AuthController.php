<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Models\UserVerify;
use Exception;
use Str;
use DB;
use Mail;
Use Alert;
use App\Jobs\SendEmailJob;
use App\Models\Blog;
use Auth;
use Hash;
class AuthController extends Controller
{
    public function loginForm()
    {
        return view('content.authentications.auth-login-basic');
    }

    public function registerForm()
    {
        return view('content.authentications.auth-register-basic');
    }

    public function login(LoginRequest $request)
    {
        try{
            $credentials = [
                'email' => $request->email,
                'password' => $request->password
            ];
            if(Auth::attempt($credentials)){
                if(!empty(Auth::User()->email_verified_at)){
                    // ->withSuccess('You have successfully logged in!');
                    alert()->toast()->success('You have successfully logged in!');
                    return redirect()->route('dashboard');
                }else{
                    alert()->toast()->warning("Please verify your email address first");
                }
                
            }else{
                alert()->toast()->error("Invalid email or password");
            }
            return redirect()->back();
        }catch(Exception $e){
            alert()->error($e->getMessage());
        }

    }

    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try{
            $user             = new User();
            $user->first_name = $request->first_name;
            $user->last_name  = $request->last_name;
            $user->dob        = date('Y-m-d',strtotime($request->dob));
            $user->contact_no = $request->contact_no;
            $user->email      = $request->email;
            $user->password   = Hash::make($request->password);
            $user->save();

            $token = Str::random(64);
  
            $userToken = new UserVerify();
            $userToken->user_id = $user->id;
            $userToken->token = $token;
            $userToken->save();
  
            // Mail::send('email.verify', ['token' => $token], function($message) use($request){
            //     $message->to($request->email);
            //     $message->subject('Email Verification Mail');
            // });
            dispatch(new SendEmailJob($token,$request->email));

            DB::commit();
            alert()->toast()->success('Register Successfully.Please check your email for verification.');

            return redirect()->route('auth-login-basic');
        }catch(Exception $e){
            DB::rollBack();
            alert()->toast()->error($e->getMessage());
        }
    }

    public function verifyAccount($token)
    {
        try{
            $verifyUser = UserVerify::where('token', $token)->first();
    
            $message = 'Sorry your email cannot be identified.';
            $type = "error";
    
            if(!is_null($verifyUser) ){
                $user = $verifyUser->user;
                
                if(!$user->is_email_verified) {
                    $verifyUser->user->email_verified_at = date('Y-m-d H:i:s');
                    $verifyUser->user->save();
                    $message = "Your e-mail is verified. You can now login.";
                    $type    = "success";
                } else {
                    $message = "Your e-mail is already verified. You can now login.";
                    $type    = "success";
                }
            }
    
            alert()->toast()->$type($message);
            return redirect()->route('auth-login-basic')->with('message', $message);
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    public function logout()
    {
        try{
            Auth::logout();

            alert()->toast()->success("Logout successfully.");

            return redirect()->route('auth-login-basic');
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    public function dashboard()
    {
        try{
            $blogCount       = Blog::count();
            $userCount       = User::count();
            $activeBlogCount = Blog::where('status',1)->count();

            return view('dashboard',compact('blogCount','userCount','activeBlogCount'));
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }
}
