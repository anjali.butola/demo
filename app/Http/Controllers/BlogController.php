<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Illuminate\Http\Request;
use Auth;
use Exception;
use Alert;
use App\Http\Traits\GeneralTrait;

class BlogController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $blogs = Blog::where('user_id',Auth::User()->id)->sortable()->latest()->paginate(10);

            $title = 'Delete Blog!';
            $text  = "Are you sure you want to delete?";
            confirmDelete($title, $text);

            return view('blogs.index',compact('blogs'));
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        try{
            $blog              = new Blog();
            $blog->name        = $request->title;
            $blog->description = $request->description;
            $blog->user_id     = Auth::User()->id;
            $blog->status      = $request->status;
            if(!empty($request->image)){
                $blog->image = $this->uploadFile($request->image,'uploads/image');
            }
            $blog->save();
            alert()->toast()->success("Blog added successfully.");
    
            return redirect()->route('blogs.index');
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $blog = Blog::find($id);
            return view('blogs.edit',compact('blog'));
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        try{
            $blog              = Blog::find($id);
            $blog->name        = $request->title;
            $blog->description = $request->description;
            $blog->status      = $request->status;
            if(!empty($request->image)){
                $blog->image = $this->uploadFile($request->image,'uploads/image');
            }
            $blog->save();

            alert()->toast()->success("Blog updated successfully.");
            
            return redirect()->route('blogs.index');
        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Blog::destroy($id);

            alert()->toast()->success("Blog deleted successfully.");

            return redirect()->route('blogs.index');

        }catch(Exception $e){
            alert()->toast()->error($e->getMessage());
        }
    }

    public function updateStatus(Request $request,$id)
    {
        try{
            $blog         = Blog::find($id);
            $blog->status = $request->status;
            $blog->save();

            return response()->json(['success'=>'Blog status updated successfully.']);

        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
}
