<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Blog extends Model
{
    use HasFactory,SoftDeletes, Sortable;

    public $sortable = ['name', 'description'];

    public $fillable = [
        'name',
        'description',
        'user_id',
        'status',
        'image'
    ];
}
// Blog::factory()->count(10)->create()