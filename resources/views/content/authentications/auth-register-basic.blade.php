@extends('layouts/blankLayout')

@section('title', 'Register')

@section('page-style')
<!-- Page -->
<link rel="stylesheet" href="{{asset('assets/vendor/css/pages/page-auth.css')}}">
@endsection
@section('content')
<div class="container-xxl">
  <div class="authentication-wrapper authentication-basic container-p-y">
    <div class="authentication-inner">

      <!-- Register Card -->
      <div class="card">
        <div class="card-body">
          <!-- Logo -->
          <div class="app-brand justify-content-center">
            {{-- <a href="{{url('/')}}" class="app-brand-link gap-2"> --}}
              {{-- <span class="app-brand-logo demo">@include('_partials.macros',["width"=>25,"withbg"=>'#696cff'])</span> --}}
              <span class="demo text-body fw-bolder">Register Form</span>
            {{-- </a> --}}
          </div>
          <!-- /Logo -->

          <form id="formAuthentication registerForm" class="mb-3" action="{{route('register')}}" method="POST">
            @csrf
            <div class="mb-3">
              <label for="first_name" class="form-label">First Name</label>
              <input type="text" class="form-control" id="first_name" name="first_name" autofocus value="{{ old('first_name') }}">
              <span class="text-danger">{{ $errors->first('first_name') }}</span>
            </div>
            <div class="mb-3">
              <label for="last_name" class="form-label">Last Name</label>
              <input type="text" class="form-control" id="last_name" name="last_name" autofocus value="{{ old('last_name') }}">
              <span class="text-danger">{{ $errors->first('last_name') }}</span>
            </div>
            <div class="mb-3">
              <label for="email" class="form-label">Email</label>
              <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
              <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="mb-3">
              <label for="contact_no" class="form-label">Contact No</label>
              <input type="text" class="form-control" id="contact_no" name="contact_no" value="{{ old('contact_no') }}" 
              onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
              >
              <span class="text-danger">{{ $errors->first('contact_no') }}</span>
            </div>
            <div class="mb-3">
              <label for="dob" class="form-label">DOB</label>
              <input type="date" class="form-control" id="dob" name="dob" value="{{ old('dob') }}">
              <span class="text-danger">{{ $errors->first('dob') }}</span>
            </div>
            <div class="mb-3 form-password-toggle">
              <label class="form-label" for="password">Password</label>
              <div class="input-group input-group-merge">
                <input type="password" id="password" class="form-control" name="password" />
                {{-- <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span> --}}
              </div>
            </div>
            <span class="text-danger">{{ $errors->first('password') }}</span>
            <div class="mb-3 form-password-toggle">
              <label class="form-label" for="confirm_password">Confirm Password</label>
              <div class="input-group input-group-merge">
                <input type="password" id="confirm_password" class="form-control" name="confirm_password" />
                {{-- <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span> --}}
              </div>
            </div>
            <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
            @if ($errors->has('g-recaptcha-response'))
              <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
            @endif
            <button class="btn btn-primary d-grid w-100">
              Sign up
            </button>
          </form>

          <p class="text-center">
            <span>Already have an account?</span>
            <a href="{{route('auth-login-basic')}}">
              <span>Sign in instead</span>
            </a>
          </p>
        </div>
      </div>
    </div>
    <!-- Register Card -->
  </div>
</div>
</div>
@endsection

@section('page-script')

<script src="https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_RECAPTCHA_KEY') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\RegisterRequest') !!}

<script>
  $(document).ready(function(){
    // $("#dob").datepicker();
    $('#registerForm').submit(function(event) {
      event.preventDefault();
  
      grecaptcha.ready(function() {
          grecaptcha.execute("{{ env('GOOGLE_RECAPTCHA_KEY') }}", {action: 'subscribe_newsletter'}).then(function(token) {
              $('#registerForm').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
              $('#registerForm').unbind('submit').submit();
          });
      });
    });
  });
  
</script>
  
@endsection

