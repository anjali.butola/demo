@extends('layouts/contentNavbarLayout')

@section('title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h4 class="fw-bold py-3 mb-4">
        <span class="text-muted fw-light">Dashboard </span>
        </h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <div class="card bg-primary text-white">
            <div class="card-body">
            <h5 class="card-title text-white">Total Users</h5>
            <p class="card-text">{{ $userCount }}</p>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="card bg-primary text-white">
            <div class="card-body">
            <h5 class="card-title text-white">Total Blogs</h5>
            <p class="card-text">{{ $blogCount }}</p>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="card bg-primary text-white">
            <div class="card-body">
            <h5 class="card-title text-white">Total Active Blogs</h5>
            <p class="card-text">{{ $activeBlogCount }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
