@extends('layouts/contentNavbarLayout')

@section('title', 'Blogs')

@section('page-style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <h4 class="fw-bold py-3 mb-4">
      <span class="text-muted fw-light">Blogs /</span> Blogs Listing
    </h4>
  </div>

  <div class="col-md-6">
    <span class="d-flex flex-row-reverse">
      <a type="button" class="btn rounded-pill btn-primary text-white" href="{{ route('blogs.create') }}">Add</a>
    </span>
  </div>
</div>

<!-- Basic Bootstrap Table -->
<div class="card">
  <h5 class="card-header">Blog</h5>
  <div class="table-responsive text-nowrap">
    <table class="table">
      <thead>
        <tr>
          <th>@sortablelink('name')</th>
          <th>@sortablelink('description')</th>
          <th>Image</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0">
        @if($blogs->count() > 0)
          @foreach ($blogs as $blog)
            <tr>
              <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{ $blog->name }}</strong></td>
              <td>@if (strlen($blog->description) > 20)
                    {{ substr($blog->description, 0, 19) . '...' }}
                  @else
                    {{ $blog->description }}
                  @endif
              </td>
              <td>
                @if(!empty($blog->image))
                  <img src="{{ asset($blog->image) }}" height="100px" width="100px"/>
                @else
                  N/A
                @endif
              </td>
              {{-- <td> --}}
                {{-- @if($blog->status == 1)
                  <a class="btn rounded-pill btn-success" href="{{ route('blogs.destroy',$blog->id) }}" data-confirm-update-status="true">Active</a>
                @else
                  <a class="btn rounded-pill btn-danger" href="{{ route('blogs.destroy',$blog->id) }}" data-confirm-update-status="true">InActive</a>
                @endif --}}
                <td>
                  {{-- <input data-url="{{ route('blogs.update-status',$blog->id) }}"
                   class="toggle-class" type="checkbox" data-onstyle="success"
                    data-offstyle="danger" data-toggle="toggle" data-on="Active"
                     data-off="InActive" {{ $blog->status ? 'checked' : '' }}> --}}
                     <input type="checkbox" class="toggle-class" data-url="{{ route('blogs.update-status',$blog->id) }}" data-onstyle="success"
                     data-offstyle="danger" data-on="Active" data-toggle="toggle"
                      data-off="InActive" {{ $blog->status == 1 ? 'checked' : '' }} />
               {{-- </td> --}}
              </td>
              <td>
                <div class="dropdown">
                  <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('blogs.edit',$blog->id) }}"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                    <a class="dropdown-item" href="{{ route('blogs.destroy',$blog->id) }}" data-confirm-delete="true"><i class="bx bx-trash me-1"></i> Delete</a>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        @else
            <center>No Records Found !!</center>
        @endif
      </tbody>
    </table>
    
  </div>
</div>
<!--/ Basic Bootstrap Table -->
<div class="mt-3">
  {!! $blogs->withQueryString()->links('pagination::bootstrap-5') !!}
</div>
@endsection

@section('page-script')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>
  $(document).ready(function(){
    $(document).on("change",".toggle-class",function(e) {
      var status = $(this).prop('checked') == true ? 1 : 0; 
      var url = $(this).data('url'); 
      // Swal.fire({
      //     title: 'Are you sure ?',
      //     text: "Do you really want to update status ?",
      //     cancelButtonText: "Cancel",
      //     icon: 'warning',
      //     showCancelButton: true,
      //     confirmButtonColor: '#3085d6',
      //     cancelButtonColor: '#d33',
      //     confirmButtonText: "OK"
      // }).then((result) => {
      //     if (result.isConfirmed) {
            $.ajax({
              type    : "GET",
              dataType: "json",
              url     : url,
              data    : {'status': status},
              success : function(data){
                if(data.success){
                  Swal.fire(data.success,'','success')
                }
              }
            });
          // }else{
          //   if(status == 1){
          //     $(".toggle-class").prop("checked",false)
          //   }else{
          //     $(".toggle-class").prop("checked",true)
          //   }
          //   // return checked ? $(this).prop('checked', false) : $(this).prop('checked', true);
          // }
      // })
    });
  });
</script>
@endsection
