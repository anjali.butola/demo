@extends('layouts/contentNavbarLayout')

@section('title', ' Add Blog')

@section('content')
<h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Blogs/</span> Create</h4>

<!-- Basic Layout -->
<div class="row">
  <div class="col-xl">
    <div class="card mb-4">
      <div class="card-header d-flex justify-content-between align-items-center">
        <h5 class="mb-0">Update Blog</h5> <small class="text-muted float-end">Blogs</small>
      </div>
      <div class="card-body">
        <form method="POST" action="{{ route('blogs.update',$blog->id) }}" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{ $blog->id }}" />
          <div class="mb-3">
            <label class="form-label" for="basic-default-fullname">Title*</label>
            <input type="text" class="form-control" id="basic-default-fullname" name="title" value="{{ old('title',$blog->name) }}"/>
          </div>
          <div class="mb-3">
            <label class="form-label" for="basic-default-company">Description*</label>
            <textarea id="basic-default-message" class="form-control" name="description">{{ old('description',$blog->description) }}</textarea>
          </div>
          <div class="mb-3">
            <label class="form-label" for="basic-default-company">Status*</label><br>
            <input type="radio" id="basic-default-fullname" name="status" value="1" checked/> Active
            <input type="radio" id="basic-default-fullname" name="status" value="0"/> InActive
          </div>
          <div class="mb-3">
            <label class="form-label" for="basic-default-company">Image</label>
            <input type="file" class="form-control" id="basic-default-fullname" name="image" accept="image/*"/>
          </div>
          @if(!empty($blog->image))
            <img src="{{ asset($blog->image) }}" height="100px" width="100px" class="mb-2"/><br>
          @endif
          <button type="submit" class="btn btn-primary">Send</button>
          <a type="button" class="btn btn-secondary" href="{{ route('blogs.index') }}">Cancel</a>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@section('page-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\BlogRequest'); !!}
@endsection