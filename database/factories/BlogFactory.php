<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Blog>
 */
class BlogFactory extends Factory
{
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $users= collect(User::all()->modelKeys());
        return [
            'name'        => $this->faker->name,
            'description' => $this->faker->text(),
            'status'      => $this->faker->boolean(),
            'user_id'     => $users->random(),
            'image'       => $this->faker->imageUrl(),    
        ];
    }
}
